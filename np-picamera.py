#!/usr/bin/env python3

#!flask/bin/python

if __name__ == '__main__':
    from app import app
    app.run(host='0.0.0.0', debug=False, threaded=True)
