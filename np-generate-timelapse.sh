#!/bin/bash

#
# Part of np-picamera distro, meant to be called w/ cron on server which the PI copies images to.
#
# example cron table entry:
#     02 30 * * * /home/cam/source/np-picamera/np-generate-timelapse.sh /home/cam/cam1 >> /tmp/np-generate-timelapse.log
#

IMAGES_BASE_DIR=$1
DATE_TIME_STR=`date +"%Y%m%d-%H%M%S"`
RETAIN_DAYS_DEFAULT=10
FFMPEG=/usr/bin/ffmpeg

delete_old_dirs()
{
    if [ -z $1 ]; then
        echo "$0: Need Dir"
    fi
    BACKUP_DIR=$1
    if [ ! -d $BACKUP_DIR ];  then
        echo "No such directory $BACKUP_DIR"
        exit 1
    fi

    if [ -z $2 ]; then
        RETAIN_DAYS=$RETAIN_DAYS_DEFAULT
    else
        RETAIN_DAYS=$2
    fi
    # need a better (i.e. more compact regex like \d{9,9}
    #DIRS_MARKED_TO_DELETE=`diff <(ls -d $BACKUP_DIR/[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] ) <(ls -d $BACKUP_DIR/[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] |tail -$RETAIN_DAYS) | grep -e "^< .*" |awk '{print $2}'`
    DIRS_MARKED_TO_DELETE=`diff <(ls -d $BACKUP_DIR/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9] ) <(ls -d $BACKUP_DIR/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9] |tail -$RETAIN_DAYS) | grep -e "^< .*" |awk '{print $2}'`
    for dir in $DIRS_MARKED_TO_DELETE
    do
        echo "Delete $dir"
        rm -rf $dir
    done
}

generate_timelapse()
{
    TIMELAPSE_DIR=$1
    echo " [-] making $TIMELAPSE_DIR/modimages"

    if ! [ -f $TIMELAPSE_DIR/TIMELAPSE_COOKIE_MODS ]; then
        mkdir -p $TIMELAPSE_DIR/modimages
        for f in `ls $TIMELAPSE_DIR/*.jpg`;
            do
            f_basename=`basename $f`
            $FFMPEG -y -i $f -vf "drawtext=fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf:text='$f':x=0:y=0:fontcolor='white':box=1:boxcolor=0x000000AA:x=10:y=10" $TIMELAPSE_DIR/modimages/mod-$f_basename;
        done
        touch $TIMELAPSE_DIR/TIMELAPSE_COOKIE_MODS
    fi

    if ! [ -e $TIMELAPSE_DIR/TIMELAPSE_COOKIE_MOVIE ]; then
        $FFMPEG -i $TIMELAPSE_DIR/modimages/%*.jpg -r 30 -q:v 2 $TIMELAPSE_DIR/timelapse.mp4
        touch $TIMELAPSE_DIR/TIMELAPSE_COOKIE_MOVIE
        rm -rf $TIMELAPSE_DIR/modimages
        rm $TIMELAPSE_DIR/TIMELAPSE_COOKIE_MODS
    fi
    touch $TIMELAPSE_DIR/TIMELAPSE_COOKIE
}
# ffmpeg -i '%*.jpg' -r 30 -q:v 2 timelapse.mp4
# for f in `ls *.jpg`; do ffmpeg -i $f -vf "drawtext=fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf:text='$f':x=0:y=0:fontcolor='white':box=1:boxcolor=0x000000AA:x=10:y=10" modimgs/mod-$f; done

if [ -z $IMAGES_BASE_DIR ] ;  then
    echo "Need dir name"
    exit 1
fi

main()
{
  for d in `ls $IMAGES_BASE_DIR |sort -n`; do
    dir_of_interest=$IMAGES_BASE_DIR/$d
    if [ $d -ne `/bin/date +"%Y%m%d"` ] ; 
    then
        if ! [ -e $dir_of_interest/TIMELAPSE_COOKIE ] ; then
            echo "Generating for dir $d"
            generate_timelapse $dir_of_interest
        else
            echo "Cookie exists for dir $d"
        fi
    else
        echo "Skipping today's dir of $d"
    fi
  done
}

main

delete_old_dirs $IMAGES_BASE_DIR

#
# ffmpeg -i '%*.jpg' -r 30 -q:v 2 timelapse.mp4
# for f in `ls *.jpg`; do ffmpeg -i $f -vf "drawtext=fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf:text='$f':x=0:y=0:fontcolor='white':box=1:boxcolor=0x000000AA:x=10:y=10" modimgs/mod-$f; done
#
