


NP Camera (for pi)
====================
I built this wireless security camera for fun. There are many pre-built solutions, but where's the fun in that? Also, the pre-built solutions have problems: https://www.google.com/search?q=googlle.&ie=utf-8&oe=utf-8#q=wireless+security+camera+vulnerabilities


based on
[video streaming with Flask](http://blog.miguelgrinberg.com/post/video-streaming-with-flask).

# Table of Contents
1. [What does it do?](#function)
  1. [Flow](#flow-chart)
2. [Hardware](#hardware)
3. [Software](#software)
4. [Start on boot](#start-on-boot)
5. [Image Data Store](#img-data-store)
6. [To Do](#to-do)

### Function <a name="function"></a>

This software will grab images from the raspberry pi camera at a configurable
frequency called **snap interval** and store them locally on the PI. After a
configurable period of of time called **copy interval** an rsync process will
run to copy images from the PI to a remove server, again configurable as
**remote image repo**, to preseve filesystem space on the PI since SD cards
will fill up quickly with images. 

A user can also connect to the PI to view a semi-steaming view of the camera.
The web page will update with an image on a configurable frequncy known as
**stream interval**. A streaming session will last for a maximum period of
known as **stream max runtime** which will prevent a streaming user from using
all the PI's resources forever. After **stream max runtime** has expired a
timeout message will appear in the stream, at which point the user can reload
the page to restart the stream.

A third component to this application is that the raspberry pi is usually very
limited to its space, so images are copied from the PI to a remote machine.
There is a supplied script meant to be called on the remote machine which will
generate a timelapse of the images collected and it will delete directories
older than a given retention time period, 10 days by default. To enable this
aspect, the script `np-generate-timelapse.sh` needs to be be installed in cron,
see [Image Data Store] (#img-data-store).

#### Thread Flow <a name="flow-chart"></a>
![Threads](doc_imgs/np-picamera-flow.png)


Hardware, Physical Box:<a name="hardware"></a>
--------------
 
 - Raspberry Pi 3
 - Adafruit Pi Box Plus Enclosure https://www.adafruit.com/product/1985
 - OV5647 Camera Module with Lens: https://www.amazon.com/Arducam-Megapixels-OV5647-Camera-Raspberry/
 - Go Pro Hero 3 Housing : https://www.amazon.com/gp/product/B00L7K51GA/
 - Go Pro 1/4-20 mount adators: https://www.amazon.com/gp/product/B00HRSBTNA/

 The hardware is comprised of the list itemized above. The raspberry pi board
 gets seated in the [Adafruit Pi Box Enclosure](https://www.adafruit.com/product/1985). The camera board gets
 installed into the [Go Pro Hear 3]
 (https://www.amazon.com/gp/product/B00L7K51GA/) with some modifications (see
 pictures below). The two cases holding the Raspberry Pi and the camera c-mount
 board get mated together using a standard 1/4-20 screw and the [Go Pro 1/4-20 mount](https://www.amazon.com/gp/product/B00HRSBTNA/).

 See pictures below for an overview of assembly. 

![Lens Mount](doc_imgs/lens_mounting_screw_notch.jpg)

![Cable Slot](doc_imgs/camera_cable_slot.jpg)

![Camera Mounted](doc_imgs/camera_mounted.jpg)

Software<a name="software"></a>
-------------------

### Install packages

Assuming working network connection to Raspberry Pi running [raspbian](https://www.raspberrypi.org/documentation/installation/installing-images/README.md). 
Enable  ssh, camera w/ raspbi-config, and probably want to change default password.

Update Package repos:

    sudo apt-get update

Install necessary packages:

    sudo apt-get install -y git supervisor python3-picamera python3-flask

Personal use packages: 

    sudo apt-get install -y zsh vim aptitude byobu 
    sudo apt-get remove nano

### Checkout source
    mkdir ~/source
    cd ~/source/
    git clone https://gitlab.com/noyez/np-picamera

### Test source
First, edit `app/config.ini`. If using a remote machine to store images, scp is used and therefor a ssh-keys need to be setup for password-less remote copy.

    cd ~/source/np-picamera
    python3 np-picamera.py

#### Tail log file
    tail -f ~/source/np-picamera/camera.log


Prepare for it to start on Boot<a name="start-on-boot"></a>
-----------------------------


### Forward 80 -> 5000

Add redirect line to `/etc/rc.local` such that Flask's defalut port 5000
redirects to port 80. `iptables -t nat -I PREROUTING --src 0/0 --dst
192.168.1.220 -p tcp --dport 80 -j REDIRECT --to-ports 5000`. Note if using
network interface other than wlan0, substitude correct network interface.

    MY_IP=`hostname -I |cut -d " " -f 1`
    sudo sed -i s/"^exit 0$"/"iptables -t nat -I PREROUTING --src 0\/0 --dst $MY_IP -p tcp --dport 80 -j REDIRECT --to-ports 5000\nexit 0"/  /etc/rc.local


### Start up boot

Configure supervisor for this app.

Example Config

/etc/supervisor/conf.d/np-picamera.conf


    [program:np-picamera]
    command = python3 np-picamera.py
    directory = /home/pi/source/np-picamera/
    autostart = true
    autorestart = true
    user = pi
    #stdout_logfile=%(directory)s/var/log/
    #stdout_logfile_maxbytes=1MB
    startretries=3

Start and starp using supervisorctl

    $ sudo supervisorctl start np-picamera
    $ sudo supervisorctl status
    np-picamera                         RUNNING    pid 598, uptime 19:51:35


#### Check log file

    tail -f ~/source/np-picamera/camera.log

Image Data Store <a name="img-data-store"></a>
------------
The np-picamera porgams perodically copies images over to a server which will
host the images for a given number of days.  Each day generates around 4.5Gb of
data, so it is not possible to store many days directly on the PI, therefor the
images are offloaded to another server. 

The script np-generate-timelapse.sh will take images in a sub-directories that
are generated by the np-picamera program. The script will also delete old
directories that are older than 10 days by default. See the script to change
history retention.

The np-generate-timelapse.sh script is meant to be called through cron for example crontab entry:

    02 30 * * * /home/cam/source/np-picamera/np-generate-timelapse.sh /home/cam/cam1 >> /tmp/np-generate-timelapse.log





To Do <a name="to-do"></a>
-------
 - organize images by the hour.
 - prevent local_imgs directory from getting copied over to remote machine.
 - reload configuration when it changes
 - explore http://www.arbetsmyra.dyndns.org/nard/ since SD cards currupt easily
