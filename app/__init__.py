#!/usr/bin/env python
from flask import Flask
import atexit
import threading
import logging
from collections import namedtuple


# Raspberry Pi camera module (requires picamera package)
from app.camera_pi import Camera

# variables that are accessible from anywhere
CameraStatus = namedtuple('CameraStatus', ['streaming', 'snap', 'in_use'])
commonDataStruct = CameraStatus(streaming=False, snap=False, in_use=False)

app = Flask(__name__)
#
# There's probably a better way to init the camera
#
cam = Camera(app.config)
cam.initialize()
Camera.stop_streaming()

from app import views

#if __name__ == '__main__':
#    app.run(host='0.0.0.0', debug=False, threaded=True)
