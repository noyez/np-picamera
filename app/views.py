from flask import Flask, render_template, Response
from . import app
from . import commonDataStruct
from .camera_pi import Camera
from .emails import send_email

import time

@app.route('/hello')
def hello():
    return "Hello, World!"

@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')

@app.route('/email')
def email():
    """Video streaming home page."""
    #app.logger.info('Info')
    return send_email()


def gen(camera):
    """Video streaming generator function."""
    #while True:
    while Camera.streaming():
        frame = camera.get_frame()
        frame_len = len(frame)
        #app.logger.info('--frame\r\n'
        #       'Content-Type: image/jpeg\r\nContent-length: '+ str(frame_len)+ ' \r\n\r\n' + "[frame data]" + '\r\n')
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\nContent-length: ' +str(frame_len).encode('UTF8')+ b'\r\n\r\n' + frame + b'\r\n')
    frame = camera.get_frame()
    frame_len = len(frame)
    #
    # Content-Type: image/jpeg
    # Content-length: 45678
    #
    #app.logger.info('final frame')
    #app.logger.info('--frame\r\n'
    #           'Content-Type: image/jpeg\r\nContent-length: '+ str(frame_len)+ ' \r\n\r\n' + "[frame data]" + '\r\n')
    #
    # return two frames here b/c safari doesn't like the last frame
    # 
    yield (b'--frame\r\n'
                b'Content-Type: image/jpeg\r\nContent-length: '+str(frame_len).encode('ascii')+ 
                b'\r\n\r\n' + frame + b'\r\n--frame\r\n' + 
                b'Content-Type: image/jpeg\r\nContent-length: '+str(frame_len).encode('ascii')+ 
                b'\r\n\r\n' + frame + b'\r\n--frame--\r\n')



@app.after_request
def after_this_request(response):
    app.logger.warning("after request")
    app.logger.warning(response)
    return response

@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    app.logger.warning("/video_feed: data %s" % (str(commonDataStruct)))
    Camera.start_streaming()
    camera_ret = gen(Camera(app.config))
    app.logger.warning("/video_feed: got camera data %s" % (str(commonDataStruct)))
    return Response(camera_ret,
                    mimetype='multipart/x-mixed-replace;boundary=frame')

