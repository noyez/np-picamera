import time
import io
import threading
import picamera
import logging
import logging.config
import os
import datetime
import re
import collections
import shlex
import subprocess
import configparser
import sys
import socket

# from: http://stackoverflow.com/questions/20913411/test-if-an-internet-connection-is-present-in-python
REMOTE_SERVER = "www.google.com"
def is_connected():
  try:
    # see if we can resolve the host name -- tells us if there is
    # a DNS listening
    host = socket.gethostbyname(REMOTE_SERVER)
    # connect to the host -- tells us if the host is actually
    # reachable
    s = socket.create_connection((host, 80), 2)
    return True
  except:
     pass
  return False

def reboot():
    Camera._logger.info("Rebooting ... ")
    reboot_cmd = "/usr/bin/sudo /sbin/reboot"
    reboot_cmd_arr = shlex.split(reboot_cmd)
    proc = subprocess.Popen(reboot_cmd_arr, stdout=subprocess.PIPE)
    proc.wait()

def restart_netconnection(iface="wlan0"):
    Camera._logger.warn("restart netconnections(%s): TODO", iface)
    if is_connected() == False:
        # a little bit extreme but i need a short term sol'n, otherwise filesystem fills up
        reboot()
    else:
        Camera._logger.warn("netconnections(%s): ok", iface)

# from : https://docs.python.org/3/howto/logging-cookbook.html#using-a-rotator-and-namer-to-customize-log-rotation-processing
#def namer(name):
#    return name + ".gz"
#
#def rotator(source, dest):
#    with open(source, "rb") as sf:
#        data = sf.read()
#        compressed = zlib.compress(data, 9)
#        with open(dest, "wb") as df:
#            df.write(compressed)
#    os.remove(source)
#
#rh = logging.handlers.RotatingFileHandler(...)
#rh.rotator = rotator
#rh.namer = namer


class Camera(object):
    _config = configparser.ConfigParser()
    _config.read("app/config.ini")
    logging.config.fileConfig("app/config.ini")
    _logger = logging.getLogger("Camera")

    _thread     = None  # background thread that reads frames from camera
    _tx_thread  = None
    _frame      = None  # current frame is stored here by background hread
    _streaming  = True
    _last_access_stream     = time.time();
    _last_access_still      = time.time();
    _first_access_stream    = time.time();
    _DATALOCK               = threading.Lock()
    _FILES_DATALOCK         = threading.Lock()
    _CAM_INIT               = threading.Lock()
    _stream_interval_time   = float(_config['camera']['STREAM_INTERVAL_TIME'])
    _stream_max_runtime     = float(_config['camera']['STREAM_MAX_RUNTIME'])
    _stream_req_timeout     = float(_config['camera']['STREAM_REQ_TIMEOUT'])
    _snap_interval_time     = float(_config['camera']['SNAP_INTERVAL_TIME'])
    _copy_interval_time     = float(_config['camera']['COPY_INTERVAL_TIME'])
    _local_img_repo         = _config['camera']['LOCAL_IMG_REPO']
    _connection_timeout     = float(_config['camera']['CONNECTION_TIMEOUT'])
    #_connection_timeout     = 30.0


    #_formatter   = logging.Formatter('%(asctime)s %(levelname)-8s -- %(message)s',  datefmt='%Y%m%d %I:%M:%S')
    #_logger      = logging.getLogger("Camera")
    #_handler     = logging.FileHandler(_config['camera']['LOG_FILE'])
    #_handler.setFormatter(_formatter)
    #_handler.setLevel(LOG_LEVEL)
    #_logger.addHandler(_handler)
    #_logger.setLevel(LOG_LEVEL)

    _remote_copy = False
    _remote_repo = _config['camera']['REMOTE_IMG_REPO']
    _remote_regex = "(?P<user>[a-zA-Z0-9_.\-^:]+?)@(?P<host>[a-zA-Z0-9_.\-^:]+?):(?P<dir>[a-zA-Z0-9_.\-^:/]+)"
    _remote = None
    _outfiles = []
    _timeout_frame = open(os.path.expanduser(_config['camera']['TIMEOUT_FRAME']), "rb").read()

    def __init__(self,conf):
        self.config = conf
        m = re.search(Camera._remote_regex, Camera._remote_repo)
        Camera._thread_tx    = threading.Thread(target=self._rsync_thread_func)
        Camera._thread_tx.start()
        if (m):
            Remote = collections.namedtuple('Remote', ['user', 'host', 'dir'])
            Camera._remote = Remote(user=m.group("user"), host=m.group("host"), dir=m.group("dir"))
            Camera._remote_copy = True
        else:
            Camera._remote = None

    def streaming():
        return Camera._streaming
    def start_streaming():
        Camera._first_access_stream = time.time();
        Camera._last_access_stream  = time.time();
        Camera._last_access_still   = time.time();
        Camera._streaming = True
        Camera._logger.warn("stream: start_streaming()")
    def stop_streaming():
        Camera._streaming = False
        Camera._logger.warn("stream: stop_streaming()")

        #Camera._logger.debug('debug message')
        #Camera._logger.info('info message')
        #Camera._logger.warn('warn message')
        #Camera._logger.error('error message')
        #Camera._logger.critical('critical message')


    def initialize(self):
        with Camera._CAM_INIT:
            #Camera._logger.info("got cam lock")
            #time.sleep(5)
            if Camera._thread is None:
                _first_access_stream = time.time()
                Camera._logger.info("init camera: no thread creating")
                # start background frame thread
                Camera._thread       = threading.Thread(target=self._camera_thread_func)
                Camera._thread.start()

            # wait until frames start to be available
            while self._frame is None:
                time.sleep(Camera._stream_interval_time)
            #Camera._logger.info("release cam lock")

    def get_frame(self):
        if (Camera._streaming):
            Camera._last_access_stream = time.time()
            self.initialize()
            #return Camera._timeout_frame
            return self._frame
        else:
            Camera._logger.debug("getting dummy frame")
            return Camera._timeout_frame
            #return self._frame

    #
    # Thread to transfer images from PI to remote machine.
    #
    @classmethod
    def _rsync_thread_func(cls):
        while True:
            Camera._logger.debug("tx thread total files:%i" % (len(Camera._outfiles)))
            if (len(Camera._outfiles) > 3):

                with Camera._FILES_DATALOCK:
                    Camera._logger.info("tx thread: copy imgs")
                    outfiles_copy       = Camera._outfiles
                    Camera._outfiles    = []


                now             = time.localtime()
                output_suffix   = time.strftime("%Y%m%d_%H%M%S", now)
                out_filename    = "img_%s.jpg" % (output_suffix)
                out_dir_timestamp = "%s" % (time.strftime("%Y%m%d", now))
                rsync_cmd       = "rsync -v -a %s %s@%s:%s/%s --rsync-path=\"mkdir -p %s && rsync\" " % (' '.join(outfiles_copy), Camera._remote.user, Camera._remote.host, Camera._remote.dir, out_dir_timestamp, Camera._remote.dir)
                Camera._logger.info("tx thread: %s" % (rsync_cmd))
                rsync_cmd_arr   = shlex.split(rsync_cmd)
                proc            = subprocess.Popen(rsync_cmd_arr, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

                timeout_seconds = Camera._connection_timeout
                Camera._logger.warn("tx thread: waiting for rsync (%0.3f s)", timeout_seconds);
                #timeout_seconds=30 # 5 min
                try:
                    proc.wait(timeout=timeout_seconds)
                except TimeoutExpired:
                    proc.kill()
                    outs,errs=proc.communicate()
                    Camera._logger.warn("tx thread: rsync timeout: stdout: %s", outs)
                    Camera._logger.warn("tx thread: rsync timeout: stderr: %s", errs)
                    restart_netconnection(iface="wlan0")
                except Exception as e:
                    Camera._logger.warn("tx thread: rsync exceoption: stderr: %s", str(e))

               	outs,errs=proc.communicate()
                Camera._logger.warn("tx thread: done waiting for rsync: %s", outs);
                Camera._logger.warn("tx thread: done waiting for rsync: %s", errs);
                Camera._logger.warn("tx thread: done waiting for rsync: %i", proc.returncode);
                if (proc.returncode != 0):
                    Camera._logger.warn("tx thread: rsync failed, trying to reset connection")
                    restart_netconnection(iface="wlan0")

                for f in outfiles_copy:
                    Camera._logger.info("tx thread: removing file %s" % (f))
                    os.remove(f)
            else:
                time.sleep(Camera._copy_interval_time)
        return

    @classmethod
    def _camera_thread_func(cls):
        with picamera.PiCamera() as camera:
          with cls._DATALOCK:
            cls._first_access_stream = time.time()
            # camera setup
            #camera.resolution = (320, 240)
            #camera.resolution = (720, 480)
            camera.resolution = (1280, 720)
            #camera.resolution = (1920, 1080)
            camera.hflip = False
            camera.vflip = False

            # let camera warm up
            camera.start_preview()
            time.sleep(2)

            stream = io.BytesIO()
            snap_delta_interval = datetime.timedelta(seconds=Camera._snap_interval_time)
            #camera.annotate_text = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            for foo in camera.capture_continuous(stream, 'jpeg', use_video_port=True):
                if (Camera._streaming):
                    time.sleep(Camera._stream_interval_time)
                else:
                    Camera._logger.debug("Snap mode: Sleeping for %i s" %(Camera._snap_interval_time));
                    # NOTE: this introduces a lag time to start streaming.
                    time.sleep(Camera._snap_interval_time)

                tDiffStill  = time.time() - Camera._last_access_still
                tDiffFirst  = time.time() - Camera._first_access_stream

                if (Camera.streaming() and (tDiffFirst > Camera._stream_max_runtime)):
                    Camera._logger.debug("stream: Streaming timeout")
                    Camera.stop_streaming()

                if (tDiffStill > Camera._snap_interval_time):
                    now = time.localtime()
                    output_suffix = time.strftime("%Y%m%d_%H%M%S", now)
                    out_filename = "img_%s.jpg" % (output_suffix)
                    out_dir_timestamp = "%s/%s" % (Camera._local_img_repo, time.strftime("%Y%m%d", now))
                    outfile = "%s%s%s" %(out_dir_timestamp, os.sep, out_filename)
                    if not os.path.exists(out_dir_timestamp):
                        Camera._logger.info("a new day, a new directory, %s" %(out_dir_timestamp))
                        os.makedirs(out_dir_timestamp)
                    write_file = open(outfile, "wb")
                    write_file.write(cls._frame)
                    write_file.close()
                    st_data = os.stat(outfile)
                    data_per_hour_bytes = (st_data.st_size / snap_delta_interval.seconds) * 60 * 60
                    #data_per_hour_bytes = (st_data.st_size / delta_interval.seconds) * 60 * 60
                    Camera._logger.info ("grabbed image ---> %s%s%s [%.1f KiB, %.1f MiB/hour, %.f MiB/day]... sleeping ..." % (
                        out_dir_timestamp,
                        os.sep,
                        out_filename, st_data.st_size/(1<<10) , data_per_hour_bytes/(1<<20),  24 * (data_per_hour_bytes/(1<<20))   ))

                    Camera._logger.debug("Snap -- total runtime = %f", tDiffFirst)
                    Camera._last_access_still = time.time()
                    with Camera._FILES_DATALOCK:
                        Camera._outfiles.append(outfile)

                # store frame
                stream.seek(0)
                cls._frame = stream.read()

                # reset stream for next frame
                stream.seek(0)
                stream.truncate()
                tDiffStream  = time.time() - Camera._last_access_stream
                if (Camera.streaming() and (tDiffStream > Camera._stream_req_timeout )):
                    Camera._logger.warn("stream: timeout on access time")
                    Camera.stop_streaming()
        cls._thread = None
